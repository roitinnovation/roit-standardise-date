import { expect } from 'chai';
import { describe, it } from 'mocha';
import MockDate from 'mockdate';
import { formatDate, Timezone } from '../../src';
import { formatDateTimeZone, retrieveDate } from '../../src/date/DateFormat';

const date = 1621356874897
MockDate.set(date)

describe('DateFormat', () => {
   it('Should be able to format dd/mm/yyyy to ISO format', () => {
      const formatDateResult = formatDate('12/01/2021')
      const retrieveDateResult = retrieveDate(formatDateResult as string)

      const formatDateExpected = '2021-01-12T00:00:00.000Z'
      const retrieveDateExpected = '2021-01-12T03:00:00.000Z'

      expect(formatDateResult).to.be.deep.equal(formatDateExpected)
      expect(retrieveDateResult).to.be.deep.equal(retrieveDateExpected)
   })

   it('Get timezones', () => {

      const dateUtc = '2021-10-26T11:28:53.808Z'

      const americaSaoPaulo = formatDateTimeZone(dateUtc, { timezone: Timezone.AMERICA_SAO_PAULO })
      const africaCairo = formatDateTimeZone(dateUtc, { timezone: Timezone.AFRICA_CAIRO })
      const americaArgentinaSanLuis = formatDateTimeZone(dateUtc, { timezone: Timezone.AMERICA_ARGENTINA_SAN_LUIS })
      const asiaAlmaty = formatDateTimeZone(dateUtc, { timezone: Timezone.ASIA_ALMATY })
      const tokio = formatDateTimeZone(dateUtc, { timezone: Timezone.ASIA_TOKYO })

      expect(tokio).to.be.deep.equal('2021-10-26T20:28:53.808Z')
      expect(asiaAlmaty).to.be.deep.equal('2021-10-26T17:28:53.808Z')
      expect(americaArgentinaSanLuis).to.be.deep.equal('2021-10-26T08:28:53.808Z')
      expect(africaCairo).to.be.deep.equal('2021-10-26T13:28:53.808Z')
      expect(americaSaoPaulo).to.be.deep.equal('2021-10-26T08:28:53.808Z')
   })
})