import { describe, it } from 'mocha';
import { expect } from "chai"

import { formatCompetence, showDateToUser, Timezone } from "../../src"

describe('DateFormat', () => {
    describe('formatCompetence()', () => {
        it('Should be able to parse a ISO format to MM/yyyy', () => {
            const result = formatCompetence('2021-02-01T22:00:00.000Z')

            const expected = '02/2021'

            expect(result).to.be.deep.equal(expected)
        })
    })

    describe('showDateToUser()', () => {
        it('Should be able to parse a ISO format to dd/MM/yyyy', () => {
            const result = showDateToUser('2021-01-12T01:00:00.000Z')

            const expected = '12/01/2021'

            expect(result).to.be.deep.equal(expected)
        })

        it('Should be able to parse a ISO format to dd/MM/yyyy HH:mm', () => {
            const result = showDateToUser('2021-01-12T01:00:00.000Z', { hours: true })

            const expected = '12/01/2021 01:00'

            expect(result).to.be.deep.equal(expected)
        })

        it('Should be able to parse a dd/MM/yyyy format to dd/MM/yyyy HH:mm', () => {
            const result = showDateToUser('2021-01-12', { hours: true })

            const expected = '12/01/2021 03:00'

            expect(result).to.be.deep.equal(expected)
        })
        
        it('Should be able to parse a new Date format to dd/MM/yyyy HH:mm', () => {
            const result = showDateToUser(new Date('10/10/2010'))

            const expected = '10/10/2010'

            expect(result).to.be.deep.equal(expected)
        })

        it('Should be able to parse a ISO format to dd/MM/yyyy using given timezone', () => {
            const result = showDateToUser('2022-05-06T01:34:07.000Z', { timezone: Timezone.AMERICA_SAO_PAULO })

            const expected = '05/05/2022'

            expect(result).to.be.deep.equal(expected)
        })
    })
})